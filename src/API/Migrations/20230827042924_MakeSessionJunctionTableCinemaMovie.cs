﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace API.Migrations
{
    public partial class MakeSessionJunctionTableCinemaMovie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_sessions",
                table: "sessions");

            migrationBuilder.DropIndex(
                name: "ix_sessions_movie_id",
                table: "sessions");

            migrationBuilder.DropColumn(
                name: "id",
                table: "sessions");

            migrationBuilder.AddPrimaryKey(
                name: "pk_sessions",
                table: "sessions",
                columns: new[] { "movie_id", "cinema_id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_sessions",
                table: "sessions");

            migrationBuilder.AddColumn<int>(
                name: "id",
                table: "sessions",
                type: "integer",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "pk_sessions",
                table: "sessions",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "ix_sessions_movie_id",
                table: "sessions",
                column: "movie_id");
        }
    }
}
