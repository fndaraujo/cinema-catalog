﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class CreateRelationSessionCinema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "cinema_id",
                table: "sessions",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "ix_sessions_cinema_id",
                table: "sessions",
                column: "cinema_id");

            migrationBuilder.AddForeignKey(
                name: "fk_sessions_cinemas_cinema_id",
                table: "sessions",
                column: "cinema_id",
                principalTable: "cinemas",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_sessions_cinemas_cinema_id",
                table: "sessions");

            migrationBuilder.DropIndex(
                name: "ix_sessions_cinema_id",
                table: "sessions");

            migrationBuilder.DropColumn(
                name: "cinema_id",
                table: "sessions");
        }
    }
}
