﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class MakeAddressDeleteBehaviorRestrict : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_cinemas_addresses_address_id",
                table: "cinemas");

            migrationBuilder.AddForeignKey(
                name: "fk_cinemas_addresses_address_id",
                table: "cinemas",
                column: "address_id",
                principalTable: "addresses",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_cinemas_addresses_address_id",
                table: "cinemas");

            migrationBuilder.AddForeignKey(
                name: "fk_cinemas_addresses_address_id",
                table: "cinemas",
                column: "address_id",
                principalTable: "addresses",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
