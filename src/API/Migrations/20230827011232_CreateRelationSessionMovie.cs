﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class CreateRelationSessionMovie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "movie_id",
                table: "sessions",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "ix_sessions_movie_id",
                table: "sessions",
                column: "movie_id");

            migrationBuilder.AddForeignKey(
                name: "fk_sessions_movies_movie_id",
                table: "sessions",
                column: "movie_id",
                principalTable: "movies",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_sessions_movies_movie_id",
                table: "sessions");

            migrationBuilder.DropIndex(
                name: "ix_sessions_movie_id",
                table: "sessions");

            migrationBuilder.DropColumn(
                name: "movie_id",
                table: "sessions");
        }
    }
}
