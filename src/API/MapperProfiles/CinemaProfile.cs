using API.DAL.DTO.Cinema;
using AutoMapper;
using Models.Cinemas;

namespace API.MapperProfiles;

public class CinemaProfile : Profile
{
    public CinemaProfile()
    {
        CreateMap<CreateCinemaDto, Cinema>();
        CreateMap<Cinema, ReadCinemaDto>()
            .ForMember(cinemaDto => cinemaDto.AddressDto, options =>
                options.MapFrom(cinema => cinema.Address)
            )
            .ForMember(cinemaDto => cinemaDto.Sessions, options =>
                options.MapFrom(cinema => cinema.Sessions)
            );
        CreateMap<UpdateCinemaDto, Cinema>();
    }
}
