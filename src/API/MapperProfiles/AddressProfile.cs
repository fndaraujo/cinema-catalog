using API.DAL.DTO.Address;
using AutoMapper;
using Models.Addresses;

namespace API.MapperProfiles;

public class AddressProfile : Profile
{
    public AddressProfile()
    {
        CreateMap<CreateAddressDto, Address>();
        CreateMap<Address, ReadAddressDto>();
        CreateMap<UpdateAddressDto, Address>();
    }
}
