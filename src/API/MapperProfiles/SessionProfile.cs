using API.DAL.DTO.Session;
using AutoMapper;
using Models.Sessions;

namespace API.MapperProfiles;

public class SessionProfile : Profile
{
    public SessionProfile()
    {
        CreateMap<CreateSessionDto, Session>();
        CreateMap<Session, ReadSessionDto>();
    }
}
