using API.DAL.DTO;
using AutoMapper;
using Models.Movies;

namespace API.MapperProfiles;

public class MovieProfile : Profile
{
    public MovieProfile()
    {
        CreateMap<CreateMovieDto, Movie>();
        CreateMap<UpdateMovieDto, Movie>();
        CreateMap<Movie, UpdateMovieDto>();
        CreateMap<Movie, ReadMovieDto>()
            .ForMember(movieDto => movieDto.Sessions, options =>
                options.MapFrom(movie => movie.Sessions)
            );
    }
}
