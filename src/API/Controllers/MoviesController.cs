using API.DAL;
using API.DAL.DTO;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Models.Movies;

namespace API.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class MoviesController : ControllerBase
{
    private readonly MovieContext _context;
    private readonly IMapper _mapper;

    public MoviesController(MovieContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Create a movie.
    /// </summary>
    /// <param name="movieDto">Movie object data</param>
    /// <returns>A created movie</returns>
    [HttpPost]
    public IActionResult CreateMovie([FromBody] CreateMovieDto movieDto)
    {
        var movie = _mapper.Map<Movie>(movieDto);
        _context.Movies?.Add(movie);
        _context.SaveChanges();
        return CreatedAtAction(nameof(GetMovieById), new { id = movie.Id}, movie);
    }

    /// <summary>
    /// Read all movies.
    /// </summary>
    /// <param name="skip">Number of records to skip</param>
    /// <param name="take">Number of records to read</param>
    /// <param name="cinemaName">Cinema name</param>
    /// <returns>A list of movies</returns>
    [HttpGet]
    public IEnumerable<ReadMovieDto> GetAllMovies([FromQuery] int skip = 0, [FromQuery] int take = 10, [FromQuery] string? cinemaName = null)
    {
        if (cinemaName is null)
        {
            var moviesQuery = _context.Movies?.Skip(skip).Take(take);
            var movieDto = _mapper.Map<List<ReadMovieDto>>(moviesQuery?.ToList());
            return movieDto;
        }
        var moviesFromLinq = _context.Movies?.Skip(skip).Take(take).Where(movie => movie.Sessions.Any(session => session.Cinema.Name == cinemaName));
        var movieDtoFromLinq = _mapper.Map<List<ReadMovieDto>>(moviesFromLinq?.ToList());
        return movieDtoFromLinq;
    }

    /// <summary>
    /// Read a movie.
    /// </summary>
    /// <param name="id">Movie Id</param>
    /// <returns>A single movie with Id</returns>
    [HttpGet("{id}")]
    public IActionResult GetMovieById(int id)
    {
        var movie = _context.Movies?.FirstOrDefault(m => m.Id == id);
        if (movie is null) return NotFound();
        var movieDto = _mapper.Map<ReadMovieDto>(movie);
        return Ok(movieDto);
    }

    /// <summary>
    /// Update a movie.
    /// </summary>
    /// <param name="movieDto">Movie object data</param>
    /// <param name="id">Movie Id</param>
    /// <returns>Nothing</returns>
    [HttpPut("{id}")]
    public IActionResult UpdateMovie([FromBody] UpdateMovieDto movieDto, int id)
    {
        var movie = _context.Movies?.FirstOrDefault(m => m.Id == id);
        if (movie is null) return NotFound();
        _mapper.Map(movieDto, movie);
        _context.SaveChanges();
        return NoContent();
    }

    /// <summary>
    /// Patch a movie.
    /// </summary>
    /// <param name="moviePatch">Movie patch</param>
    /// <param name="id">Movie Id</param>
    /// <returns>Nothing</returns>
    [HttpPatch("{id}")]
    public IActionResult PatchMovie(JsonPatchDocument<UpdateMovieDto> moviePatch, int id)
    {
        var movie = _context.Movies?.FirstOrDefault(m => m.Id == id);
        if (movie is null) return NotFound();
        var movieUpdate = _mapper.Map<UpdateMovieDto>(movie);
        moviePatch.ApplyTo(movieUpdate, ModelState);
        if (!TryValidateModel(movieUpdate)) return ValidationProblem(ModelState);
        _mapper.Map(movieUpdate, movie);
        _context.SaveChanges();
        return NoContent();
    }

    /// <summary>
    /// Delete a movie.
    /// </summary>
    /// <param name="id">Movie Id</param>
    /// <returns>Nothing</returns>
    [HttpDelete("{id}")]
    public IActionResult DeleteMovie(int id)
    {
        var movie = _context.Movies?.FirstOrDefault(m => m.Id == id);
        if (movie is null) return NotFound();
        _context.Remove(movie);
        _context.SaveChanges();
        return NoContent();
    }
}
