using API.DAL;
using API.DAL.DTO.Session;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Models.Sessions;

namespace API.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class SessionsController : ControllerBase
{
    private readonly MovieContext _context;
    private readonly IMapper _mapper;

    public SessionsController(MovieContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Create a session.
    /// </summary>
    /// <param name="sessionDto">Session object data</param>
    /// <returns>A created session</returns>
    [HttpPost]
    public IActionResult CreateSession(CreateSessionDto sessionDto)
    {
        var session = _mapper.Map<Session>(sessionDto);
        _context.Sessions?.Add(session);
        _context.SaveChanges();
        return CreatedAtAction(nameof(GetSessionById), new { movieId = session.MovieId, cinemaId = session.CinemaId}, session);
    }

    /// <summary>
    /// Read all sessions.
    /// </summary>
    /// <returns>A list of sessions</returns>
    [HttpGet]
    public IEnumerable<ReadSessionDto> ReadAllSessions()
    {
        return _mapper.Map<List<ReadSessionDto>>(_context.Sessions?.ToList());
    }

    /// <summary>
    /// Read a single session.
    /// </summary>
    /// <param name="movieId">Movie Id</param>
    /// <param name="cinemaId">Cinema Id</param>
    /// <returns>A single session with movie Id and cinema Id</returns>
    [HttpGet("{movieId}/{cinemaId}")]
    public IActionResult GetSessionById(int movieId, int cinemaId)
    {
        var session = _context.Sessions?.FirstOrDefault(s => s.MovieId == movieId && s.CinemaId == cinemaId);
        if (session is null) return NotFound();
        var sessionDto = _mapper.Map<ReadSessionDto>(session);
        return Ok(sessionDto);
    }
}
