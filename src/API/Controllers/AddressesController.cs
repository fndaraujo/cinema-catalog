using API.DAL;
using API.DAL.DTO.Address;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Models.Addresses;

namespace API.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class AddressesController : ControllerBase
{
    private readonly MovieContext _context;
    private readonly IMapper _mapper;

    public AddressesController(MovieContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Create an address.
    /// </summary>
    /// <param name="addressDto">Address object data</param>
    /// <returns>A created address</returns>
    [HttpPost]
    public IActionResult CreateAddress([FromBody] CreateAddressDto addressDto)
    {
        var address = _mapper.Map<Address>(addressDto);
        _context.Addresses.Add(address);
        _context.SaveChanges();
        return CreatedAtAction(nameof(GetAddressById), new { id = address.Id }, address);
    }

    /// <summary>
    /// Read all addresses.
    /// </summary>
    /// <param name="skip">Number of records to skip</param>
    /// <param name="take">Number of records to read</param>
    /// <returns>A list of addresses</returns>
    [HttpGet]
    public IEnumerable<ReadAddressDto> GetAllAddresses([FromQuery] int skip = 0, [FromQuery] int take = 10)
    {
        var addressQuery = _context.Addresses.Skip(skip).Take(take);
        var addressDto = _mapper.Map<List<ReadAddressDto>>(addressQuery);
        return addressDto;
    }

    /// <summary>
    /// Read an address.
    /// </summary>
    /// <param name="id">Address Id</param>
    /// <returns>A single address with Id</returns>
    [HttpGet("{id}")]
    public IActionResult GetAddressById(int id)
    {
        var address = _context.Addresses.FirstOrDefault(a => a.Id == id);
        if (address is null) return NotFound();
        var addressDto = _mapper.Map<ReadAddressDto>(address);
        return Ok(addressDto);
    }

    /// <summary>
    /// Update an address.
    /// </summary>
    /// <param name="addressDto">Address object data</param>
    /// <param name="id">Address Id</param>
    /// <returns>Nothing</returns>
    [HttpPut("{id}")]
    public IActionResult UpdateAddress([FromBody] UpdateAddressDto addressDto, int id)
    {
        var address = _context.Addresses.FirstOrDefault(a => a.Id == id);
        if (address is null) return NotFound();
        _mapper.Map(addressDto, address);
        _context.SaveChanges();
        return NoContent();
    }

    /// <summary>
    /// Patch an address.
    /// </summary>
    /// <param name="addressPatch">Address patch</param>
    /// <param name="id">Address Id</param>
    /// <returns>Nothing</returns>
    [HttpPatch("{id}")]
    public IActionResult PatchAddress(JsonPatchDocument<UpdateAddressDto> addressPatch, int id)
    {
        var address = _context.Addresses.FirstOrDefault(a => a.Id == id);
        if (address is null) return NotFound();
        var addressUpdate = _mapper.Map<UpdateAddressDto>(address);
        addressPatch.ApplyTo(addressUpdate, ModelState);
        if (!TryValidateModel(addressUpdate)) return ValidationProblem(ModelState);
        _mapper.Map(addressUpdate, address);
        _context.SaveChanges();
        return NoContent();
    }

    /// <summary>
    /// Delete an address.
    /// </summary>
    /// <param name="id">Address Id</param>
    /// <returns>Nothing</returns>
    [HttpDelete("{id}")]
    public IActionResult DeleteAddress(int id)
    {
        var address = _context.Addresses.FirstOrDefault(a => a.Id == id);
        if (address is null) return NotFound();
        _context.Remove(address);
        _context.SaveChanges();
        return NoContent();
    }
}
