using API.DAL;
using API.DAL.DTO.Cinema;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Cinemas;

namespace API.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class CinemasController : ControllerBase
{
    private readonly MovieContext _context;
    private readonly IMapper _mapper;

    public CinemasController(MovieContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    /// <summary>
    /// Create a cinema.
    /// </summary>
    /// <param name="cinemaDto">Cinema object data</param>
    /// <returns>A created cinema</returns>
    [HttpPost]
    public IActionResult CreateCinema([FromBody] CreateCinemaDto cinemaDto)
    {
        var cinema = _mapper.Map<Cinema>(cinemaDto);
        _context.Cinemas?.Add(cinema);
        _context.SaveChanges();
        return CreatedAtAction(nameof(GetCinemaById), new { id = cinema.Id }, cinema);
    }

    /// <summary>
    /// Read all cinemas
    /// </summary>
    /// <param name="skip">Number of records to skip</param>
    /// <param name="take">Number of records to read</param>
    /// <param name="addressId">Address Id</param>
    /// <returns>A list of cinemas</returns>
    [HttpGet]
    public IEnumerable<ReadCinemaDto> GetAllCinemas([FromQuery] int skip = 0, [FromQuery] int take = 10, [FromQuery] int? addressId = null)
    {
        if (addressId is null)
        {
            var cinemaQuery = _context.Cinemas?.Skip(skip).Take(take);
            var cinemaDto = _mapper.Map<List<ReadCinemaDto>>(cinemaQuery?.ToList());
            return cinemaDto;
        }
        var queryString = $"SELECT id, name, address_id FROM cinemas WHERE cinemas.address_id = {addressId}";
        var fromSqlRaw = _context.Cinemas?.FromSqlRaw(queryString);
        var cinemaFromSqlRaw = _mapper.Map<List<ReadCinemaDto>>(fromSqlRaw?.ToList());
        return cinemaFromSqlRaw;
    }

    /// <summary>
    /// Read a cinema.
    /// </summary>
    /// <param name="id">Cinema Id</param>
    /// <returns>A single cinema with Id</returns>
    [HttpGet("{id}")]
    public IActionResult GetCinemaById(int id)
    {
        var cinema = _context.Cinemas?.FirstOrDefault(c => c.Id == id);
        if (cinema is null) return NotFound();
        var cinemaDto = _mapper.Map<ReadCinemaDto>(cinema);
        return Ok(cinemaDto);
    }

    /// <summary>
    /// Update a cinema.
    /// </summary>
    /// <param name="cinemaDto">Cinema object data</param>
    /// <param name="id">Cinema Id</param>
    /// <returns>Nothing</returns>
    [HttpPut("{id}")]
    public IActionResult UpdateCinema([FromBody] UpdateCinemaDto cinemaDto, int id)
    {
        var cinema = _context.Cinemas?.FirstOrDefault(c => c.Id == id);
        if (cinema is null) return NotFound();
        _mapper.Map(cinemaDto, cinema);
        _context.SaveChanges();
        return NoContent();
    }

    /// <summary>
    /// Patch a cinema.
    /// </summary>
    /// <param name="cinemaPatch">Cinema patch</param>
    /// <param name="id">Cinema Id</param>
    /// <returns>Nothing</returns>
    [HttpPatch("{id}")]
    public IActionResult PatchCinema(JsonPatchDocument<UpdateCinemaDto> cinemaPatch, int id)
    {
        var cinema = _context.Cinemas?.FirstOrDefault(c => c.Id == id);
        if (cinema is null) return NotFound();
        var cinemaUpdate = _mapper.Map<UpdateCinemaDto>(cinema);
        cinemaPatch.ApplyTo(cinemaUpdate, ModelState);
        if (!TryValidateModel(cinemaUpdate)) return ValidationProblem(ModelState);
        _mapper.Map(cinemaUpdate, cinema);
        _context.SaveChanges();
        return NoContent();
    }

    /// <summary>
    /// Delete a cinema.
    /// </summary>
    /// <param name="id">Cinema Id</param>
    /// <returns>Nothing</returns>
    [HttpDelete("{id}")]
    public IActionResult DeleteCinema(int id)
    {
        var cinema = _context.Cinemas?.FirstOrDefault(c => c.Id == id);
        if (cinema is null) return NotFound();
        _context.Remove(cinema);
        _context.SaveChanges();
        return NoContent();
    }
}
