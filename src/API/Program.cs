using System.Reflection;
using API.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Define database connection values
var connectionName = "ConnectionStrings:DefaultConnection";
var connectionConfiguration = builder.Configuration[connectionName];

// Add services to the container.
builder.Services.AddDbContext<MovieContext>(options => options
    .UseLazyLoadingProxies()
    .UseNpgsql(connectionConfiguration)
    .UseSnakeCaseNamingConvention()
);

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddControllers().AddNewtonsoftJson();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo()
    {
        Title = "Cinema Catalog API",
        Description = "An API providing a Movie catalog through a list of Cinemas",
        Contact = new OpenApiContact()
        {
            Name = "Fernando Araujo dos Santos",
            Email = String.Empty,
            Url = new Uri("https://gitlab.com/fndaraujo")
        },
        License = new OpenApiLicense()
        {
            Name = "MIT License",
            Url = new Uri("https://gitlab.com/fndaraujo/cinema-catalog/-/blob/master/LICENSE"),
        },
        TermsOfService = new Uri("https://gitlab.com/fndaraujo/cinema-catalog/-/blob/master/LICENSE"),
        Version = "v1"
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
