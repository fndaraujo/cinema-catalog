namespace API.DAL.DTO.Address;

public class ReadAddressDto
{
    public int Id { get; set; }
    public string Street { get; set; } = String.Empty;
    public int Number { get; set; }
}
