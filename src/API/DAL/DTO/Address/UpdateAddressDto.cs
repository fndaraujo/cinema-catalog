using System.ComponentModel.DataAnnotations;

namespace API.DAL.DTO.Address;

public class UpdateAddressDto
{
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(64, ErrorMessage = "{0} should be less than or equal to {1} characters.")]
    public string Street { get; set; } = String.Empty;
    [Required(ErrorMessage = "{0} is required.")]
    [Range(1, 10000, ErrorMessage = "{0} should be between {1} and {2}.")]
    public int Number { get; set; }
}
