namespace API.DAL.DTO.Session;

public class ReadSessionDto
{
    public int MovieId { get; set; }
    public int CinemaId { get; set; }
}
