using System.ComponentModel.DataAnnotations;

namespace API.DAL.DTO.Cinema;

public class UpdateCinemaDto
{
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(64, ErrorMessage = "{0} should be less than or equal to {1} characters.")]
    public string Name { get; set; } = String.Empty;
}
