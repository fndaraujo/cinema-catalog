using API.DAL.DTO.Address;
using API.DAL.DTO.Session;

namespace API.DAL.DTO.Cinema;

public class ReadCinemaDto
{
    public int Id { get; set; }
    public string Name { get; set; } = String.Empty;

    public ReadAddressDto? AddressDto { get; set; }

    public ICollection<ReadSessionDto>? Sessions { get; set; }
}
