using API.DAL.DTO.Session;

namespace API.DAL.DTO;

public class ReadMovieDto
{
    public string Title { get; set; } = String.Empty;
    public string Genre { get; set; } = String.Empty;
    public int Duration { get; set; }
    public int Release { get; set; }

    public ICollection<ReadSessionDto>? Sessions { get; set; }

    public DateTime ConsultedAt { get; set; } = DateTime.Now;
}
