using System.ComponentModel.DataAnnotations;
using Models.Cinemas;
using Models.Movies;

namespace Models.Sessions;

public class Session
{
    [Required(ErrorMessage = "{0} is required.")]
    public int MovieId { get; set; }
    public virtual Movie? Movie { get; set; }

    [Required(ErrorMessage = "{0} is required.")]
    public int CinemaId { get; set; }
    public virtual Cinema? Cinema { get; set; }
}
