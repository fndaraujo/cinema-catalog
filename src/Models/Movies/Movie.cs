using System.ComponentModel.DataAnnotations;
using Models.Sessions;

namespace Models.Movies;

public class Movie
{
    [Key]
    [Required(ErrorMessage = "{0} is required.")]
    public int Id { get; set; }
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(64, ErrorMessage = "{0} should be less than or equal to {1} characters.")]
    public string Title { get; set; } = String.Empty;
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(16, ErrorMessage = "{0} should be less than or equal to {1} characters.")]
    public string Genre { get; set; } = String.Empty;
    [Required(ErrorMessage = "{0} is required.")]
    [Range(60, 360, ErrorMessage = "{0} should be between {1} and {2} minutes.")]
    public int Duration { get; set; }
    [Required(ErrorMessage = "{0} is required.")]
    [Range(1900, 2099, ErrorMessage = "{0} should be between dates {1} and {2}.")]
    public int Release { get; set; }

    public virtual ICollection<Session>? Sessions { get; set; }
}
