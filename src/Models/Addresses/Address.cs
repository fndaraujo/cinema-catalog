using System.ComponentModel.DataAnnotations;
using Models.Cinemas;

namespace Models.Addresses;

public class Address
{
    [Key]
    [Required(ErrorMessage = "{0} is required.")]
    public int Id { get; set; }
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(64, ErrorMessage = "{0} should be less than or equal to {1} characters.")]
    public string Street { get; set; } = String.Empty;
    [Required(ErrorMessage = "{0} is required.")]
    [Range(1, 10000, ErrorMessage = "{0} should be between {1} and {2}.")]
    public int Number { get; set; }

    public virtual Cinema? Cinema { get; set; }
}
