using System.ComponentModel.DataAnnotations;
using Models.Addresses;
using Models.Sessions;

namespace Models.Cinemas;

public class Cinema
{
    [Key]
    [Required(ErrorMessage = "{0} is required.")]
    public int Id { get; set; }
    [Required(ErrorMessage = "{0} is required.")]
    [StringLength(64, ErrorMessage = "{0} should be less than or equal to {1} characters.")]
    public string Name { get; set; } = String.Empty;

    public int AddressId { get; set; }
    public virtual Address? Address { get; set; }

    public virtual ICollection<Session>? Sessions { get; set; }
}
