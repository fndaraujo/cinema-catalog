# CINEMA CATALOG

An API providing a catalog of movies through a list of fictional cinemas.

This project follows a series of API development lessons for the .NET Core
framework in the [Alura](https://www.alura.com.br/) online courses.

## Technology

* [ASP .NET CORE 6.0](https://dotnet.microsoft.com/en-us/apps/aspnet)
* [Postgres 15](https://www.postgresql.org/)

* [Microsoft Entity Framework Core](https://github.com/dotnet/efcore)

## License

[MIT](./LICENSE)

## Authors

See [AUTHORS](./AUTHORS) file.

## Contributors

See [CONTRIBUTORS](./CONTRIBUTORS) file.
